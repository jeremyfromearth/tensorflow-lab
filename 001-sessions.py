import tensorflow as tf
"""
Simple examples demonstrating how to create a tf.Session
"""

x = tf.Variable(3, name='x')
y = tf.Variable(4, name='y')
f = x * x * y + y + 2

print('------------ Variables -------------')
print('x', x)
print('y', y)
print('f', f)

# Naive/Verbose approach evaluating f
print('------------ Naive/Verbose -------------')
sess = tf.Session()
sess.run(x.initializer)
sess.run(y.initializer)
result = sess.run(f)
sess.close()
print('Result', result)

# Using `with` to make things cleaner
print('------------ Using `with`  -------------')
with tf.Session() as s:
  x.initializer.run()
  y.initializer.run()
  r = f.eval()
  print('Result', r)

# A more concise approach allows us to automatically initialize global vars
print('------------ Using global_variables_initializer -------------')
init = tf.global_variables_initializer()
with tf.Session() as s:
  init.run()
  result = f.eval()
  print('Result', result)

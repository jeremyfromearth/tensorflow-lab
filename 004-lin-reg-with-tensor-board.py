import sys
import numpy as np
import tensorflow as tf
from datetime import datetime
from sklearn.datasets import fetch_california_housing
from sklearn.preprocessing import StandardScaler

"""
Same basic linear regression model as file 002-...py
This time uses mini batch, with placeholders and feed dicts
Input normalization via ScikitLearn and randomization via numpy
Trained model saved to disk
"""

# Load, randomize and normalize the data
housing = fetch_california_housing()
m, n = housing.data.shape
np.random.shuffle(housing.data)

scaler = StandardScaler()
normalized_housing_data = scaler.fit_transform(housing.data)
normalized_housing_data_plus_bias = np.c_[np.ones((m, 1)), normalized_housing_data]

# Hyperparameters
n_epochs = 5
batch_size = 100
n_batches = int(np.ceil(m / batch_size))
learning_rate = 0.01

# Model components
X = tf.placeholder(tf.float32, shape=(None, n + 1), name='X')
y = tf.placeholder(tf.float32, shape=(None, 1), name='y')
theta = tf.Variable(tf.random_uniform([n + 1, 1], -1.0, 1.0), name='theta')
y_pred = tf.matmul(X, theta, name="predictions")
error = y_pred - y
mse = tf.reduce_mean(tf.square(error), name='mse')

# Gradient descent with autodiff
gradients = tf.gradients(mse, [theta])[0]
optimizer = tf.train.GradientDescentOptimizer(learning_rate=learning_rate)
training_op = optimizer.minimize(mse)

# Logging info
now = datetime.utcnow().strftime('%Y%m%d%H%M%S')
logdir = './logs/{}/'.format(now)
mse_summary = tf.summary.scalar('MSE', mse)
file_writer = tf.summary.FileWriter(logdir, tf.get_default_graph())

def fetch_batch(batch_index, batch_size):
  """
  Returns a batch of training samples
  """
  i = (batch_index * batch_size)
  j = min(i + batch_size, m)
  X_batch = normalized_housing_data_plus_bias[i:j]
  y_batch = housing.target.reshape(-1, 1)[i:j]
  return X_batch, y_batch

print('Training...')
init = tf.global_variables_initializer()
with tf.Session() as s:
  s.run(init)

  for epoch in range(n_epochs):
    for batch_index in range(n_batches):
      X_batch, y_batch = fetch_batch(batch_index, batch_size)
      if batch_index % 10 == 0:
        summary_str = mse_summary.eval(feed_dict={X: X_batch, y: y_batch})
        step = epoch * n_batches + batch_index
        file_writer.add_summary(summary_str, step)
      s.run(training_op, feed_dict={X: X_batch, y: y_batch})

  best_theta = theta.eval()
  print(best_theta)

  file_writer.close()

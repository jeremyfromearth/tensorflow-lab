import numpy as np
import tensorflow as tf
from sklearn.datasets import fetch_california_housing

"""
A basic linear regression model
"""

housing = fetch_california_housing()
m, n = housing.data.shape
housing_data_plus_bias = np.c_[np.ones((m, 1)), housing.data]
normalized_housing_data = housing_data_plus_bias / housing_data_plus_bias.sum(axis=1, keepdims=True)

# Computes the Normal Equation or closed form solution
# for calculating weights of a linear regression model
print('Computing the Normal Equation')
X = tf.constant(housing_data_plus_bias, dtype=tf.float32, name='X')
y = tf.constant(housing.target.reshape(-1, 1), dtype=tf.float32, name='y')
XT = tf.transpose(X)
theta = tf.matmul(tf.matmul(tf.linalg.inv(tf.matmul(XT, X)), XT), y)

with tf.compat.v1.Session() as s:
  theta = theta.eval()
  print('Model Weights:')
  print(theta)

# Linear Regression using manually created gradients or auto-diff
n_epochs = 1000
learning_rate = 0.0035

tf.reset_default_graph()
X = tf.constant(normalized_housing_data, dtype=tf.float32, name='X')
y = tf.constant(housing.target.reshape(-1, 1), dtype=tf.float32, name='y')
theta = tf.Variable(tf.random_uniform([n + 1, 1], -1.0, 1.0), name='theta')
y_pred = tf.matmul(X, theta, name='predictions')
error = y_pred - y
mse = tf.reduce_mean(tf.square(error), name='mse')

# Manually computed gradients
# gradients = 2/m * tf.matmul(tf.transpose(X), error)

# Gradients calculated with autodiff
gradients = tf.gradients(mse, [theta])[0]

# Manually created optimization
# training_op = tf.assign(theta, theta - learning_rate * gradients)

# Optimization function provided by Tensorflow
# optimizer = tf.train.GradientDescentOptimizer(learning_rate=learning_rate)
optimizer = tf.train.MomentumOptimizer(learning_rate=learning_rate, momentum=0.9)
training_op = optimizer.minimize(mse)

# Initialize the graph
init = tf.global_variables_initializer()
print('Training')
with tf.compat.v1.Session() as s:
  s.run(init)
  for e in range(n_epochs):
    if e % 100 == 0:
      print('Epoch {}: MSE = {}'.format(e, mse.eval()))
    s.run(training_op)
  best_theta = theta.eval()
  print(best_theta)

print('Complete!')
